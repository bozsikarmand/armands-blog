---
author: bozsikarmand
comments: true
date: 2021-09-17 20:11:39+00:00
layout: post
slug: dijnyertes-muforditasom
title: Díjnyertes műfordításom
wordpress_id: 324
categories:
- Translation
---

Kedves Olvasó!

Jelen bejegyzés keretei közt egy régebbi, még középiskolás éveim elején készült műfordításomat mutatnám be, mellyel az iskolám által szervezett fordítói versenyen első helyezést értem el. 

A dokumentumon megtalálható fordításom mellett az eredeti angol nyelvű szöveg, melyről a fordítást végeztem a versenyen, illetve összehasonlításképp Szőllősy Klára alkotása is.

![A dokumentum első oldala](https://armands.blog/images/oz_translation_page_1.jpg)
![A dokumentum első oldala](https://armands.blog/images/oz_translation_page_2.jpg)